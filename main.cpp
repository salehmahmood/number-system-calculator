/**
 * 
 * 
 * Submitted to Ma'am Madiha Rasool <madiha.rasool@superior.edu.pk>
 *
 * File created by Saleh Mahmood
 * File created at January 2, 2019 9:14 PM
 */
#include <iostream>
#include <conio.h>
#include <string>
#include <stdlib.h> // exit
#include <bits/stdc++.h> // provides the reverse function for string in order to avoid the use of array

using namespace std;

// Decalring Functions
string convertFromDecimal(int decimal, int base);
int convertFromBinaryOrOctalToDecimal(int number, int base);
int convertFromHexToDecimal(string hexadecimalNumber);
bool validateInputForSelectedBase(int input);

// Main function
int main ()
{
	// Creating a start label in case to use goto
	start:
		
	// Printing Welcome Message
	cout << "+---------------------------------------------------------------+" << endl;
	cout << "|------------- Cross Number System Base Calculator -------------|" << endl;
	cout << "+---------------------------------------------------------------+" << endl << endl;
	
	cout << "This program allows you to convert any number from any base to \nany other base. Use it well! \n";
	
	// Decalaring vars; [nothing special; int would do fine]
	int userSelectedBase = 3; // Base to calculate everyting upon [1: Binary] [2: Octal] [3: Decimal] [4: Hexadecimal]
	int userInput = 0;
	string userInputHex;
	
	cout << "Please select the base from which you'd like to convert. \n\n"; // Just for end user

	// Printing available bases as options
	cout << "(1):Binary AKA base of 2\n";
	cout << "(2):Octal AKA base of 8\n";
	cout << "(3):Decimal AKA base of 10\n";
	cout << "(4):Hexadecimals AKA base of 16\n";
	
	cout << endl << "Please choose from 1: ";
	cin >> userSelectedBase;
	cout << endl;
	
	if(userSelectedBase < 1 || userSelectedBase > 4) 
	{
		cout << "The number system base you selected is not supportted.";
	} else
	{
		switch(userSelectedBase) 
		{
			case 1: cout << "Selected base: Binary\n"; break;
			case 2: cout << "Selected base: Octal\n"; break;
			case 3: cout << "Selected base: Decimal\n"; break;
			case 4: cout << "Selected base: Hexadecimals\n"; break;
		}
	
		cout << "\nPlease enter your desired number: ";
		if(userSelectedBase == 4) 
		{
			cin >> userInputHex;
		} else 
		{
			cin >> userInput;	
		}
	}
		
	// printing converted
	switch(userSelectedBase)
	{
		case 1:
		{
			int decimal = convertFromBinaryOrOctalToDecimal(userInput, 2);
			cout << "---------------------------------------------------------------";
			cout << "\nDecimal for     " << userInput << " is: " << decimal;
			cout << "\nOctal for       " << userInput << " is: " << convertFromDecimal(decimal, 8);
			cout << "\nHexadecimal for " << userInput << " is: " << convertFromDecimal(decimal, 16);
			
			break;
		}
		case 2:
		{
			int decimal = convertFromBinaryOrOctalToDecimal(userInput, 8);
			
			cout << "\nBinary for       " << userInput << " is: " << convertFromDecimal(decimal, 2);
			cout << "\nDecimal for     " << userInput << " is: " << decimal;
			cout << "\nHexadecimal for " << userInput << " is: " << convertFromDecimal(decimal, 16);
			
			break;
		}
		case 3:
		{
			cout << "\nBinary for " << userInput << " is: " << convertFromDecimal(userInput, 2);
			cout << "\nOctal for " << userInput << " is: " << convertFromDecimal(userInput, 8);
			cout << "\nHexadecimal for " << userInput << " is: " << convertFromDecimal(userInput, 16);
			
			break;
		}
		case 4:
		{
			int decimal = convertFromHexToDecimal(userInputHex);
			
			cout << "\nBinary for " << userInput << " is: " << convertFromDecimal(decimal, 2);
			cout << "\nOctal for " << userInput << " is: " << convertFromDecimal(decimal, 8);
			cout << "\nDecimal for " << userInput << " is: " << decimal;
			
			break;
		}
		default:
		{
			break;
		}
	}
	
	cout << "\n\nI hope that you found what you were looking for.\n";
	cout << "Thanks for using this Calculator.\n";

	char start;
	cout << "\nWould you like to restart the program. (y/n): ";
	cin >> start;
	
	if (start == 'y')
	{
		system("cls");
		start = ' ';
		goto start;
	} else 
	{
		exit(0);
	}
	
	return 0;
}

string convertFromDecimal(int decimal, int base) 
{
	string converted = "";
	
	/*
	 * Following switch statement converts a given 
	 * decimal number [(int)decimal] to given base [(int)base]
	 */
	switch(base) 
	{
		case 2: // To binary
			for(int i = 0; decimal > 0; decimal /= 2)
			{
				converted += to_string(decimal%2);
			}
			
			break;
		case 8: // To octal
			for(int i = 0; decimal > 0; decimal = decimal /= 8)
			{
				converted += to_string(decimal%8);
			}
			
			break;
		case 16: // To hexadecimal
			for(int i = 0; decimal > 0; decimal = decimal /= 16) 
			{
				int temp = decimal%16;
				switch(temp)
				{
					case 10: converted += "A"; break;
					case 11: converted += "B"; break;
					case 12: converted += "C"; break;
					case 13: converted += "D"; break;
					case 14: converted += "E"; break;
					case 15: converted += "F"; break;
					default: converted += to_string(temp); break;
				}
			}
			
			break;
		default: 
			break;
	}
	
	// reversing the string to get the correct answer
	reverse(converted.begin(), converted.end());
	
	return converted; // returing final calculated conversion
}

int convertFromBinaryOrOctalToDecimal(int number, int base) 
{
	if(base != 2 && base != 8)
		return 0;
	
	int decimalNumber = 0, rem;
	
    for (int i = 0; number > 0; i++)
	{
        rem = number % 10; // extracts the last digit of the given number
        number /= 10; // this will remove the last digin
        decimalNumber += rem * pow(base, i); // finaly adding the last digit
    }
    
    return decimalNumber;
}

int convertFromHexToDecimal(string hexadecimalNumber)
{
	int decimalNumber = 0, pwr, eachHexDigit;
	pwr = hexadecimalNumber.size()-1;
	
	for(int i = 0; i < hexadecimalNumber.size(); i++)
	{
		string eachHexDigitChar = hexadecimalNumber.substr(i, 1);
		int eachHexDigit;
		
		if(eachHexDigitChar == "A" || eachHexDigitChar == "a") eachHexDigit = 10;
		else if(eachHexDigitChar == "B" || eachHexDigitChar == "b") eachHexDigit = 11;
		else if(eachHexDigitChar == "C" || eachHexDigitChar == "c") eachHexDigit = 12;
		else if(eachHexDigitChar == "D" || eachHexDigitChar == "d") eachHexDigit = 13;
		else if(eachHexDigitChar == "E" || eachHexDigitChar == "e") eachHexDigit = 14;
		else if(eachHexDigitChar == "F" || eachHexDigitChar == "f") eachHexDigit = 15;
		else eachHexDigit = stoi(eachHexDigitChar);
		
		decimalNumber += eachHexDigit * pow(16, pwr);
		pwr--;
	}
	
	return decimalNumber;
}
